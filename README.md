#
This library helps you read the properties you have in your project without dealing with Exceptions control, the method to use for accessing simple properties files or any configuration in general.

<h1> Usage </h1>
The simplest way to use it is just do:
<code>
PropertiesManager.getProperty("myProperty")
</code>

to obtain any of your properties. If you have any common .properties file the library will read it for you, if you have a spectial naming you will have to specify, you can do it in several ways but the simplest is:

<code>
PropertiesManager.getProperty("myProperty","myPropertiesFileWithCustomName.properties")
</code>