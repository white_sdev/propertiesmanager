package org.white_sdev.propertiesmanager.model.service;

import org.white_sdev.propertiesmanager.model.service.strategy.PropertiesFileRetrievingStrategy;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.white_sdev.propertiesmanager.exception.PropertiesManagerException;
import org.white_sdev.propertiesmanager.model.service.strategy.PropertiesFileRetrievingThroughClassStrategy;
import org.white_sdev.propertiesmanager.model.service.strategy.impl.ClassLoaderStrategy;
import org.white_sdev.propertiesmanager.model.service.strategy.impl.ClazzStreamStrategy;
import org.white_sdev.propertiesmanager.model.service.strategy.impl.CustomLoaderStrategy;
import org.white_sdev.propertiesmanager.model.service.strategy.impl.FileInputStreamStrategy;
import org.white_sdev.propertiesmanager.model.service.strategy.impl.OutOfBuildStrategy;
import static org.white_sdev.white_validations.parameters.ParameterValidator.notNullValidation;

/**
 *
 * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
 * @since Feb 9, 2019
 */
@Slf4j
public class PropertiesManager {

    //<editor-fold defaultstate="collapsed" desc="Attributes">

    /**
     * Default class logger.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     */
    public static Logger logger = LoggerFactory.getLogger(PropertiesManager.class);

    /**
     * Common file names to look for in case they are needed.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     */
    public static Set<String> commonPropertiesFileNames = new HashSet<>() {
	{
	    add("init.properties");
	    add("config.properties");
	    add("server.properties");
	    add("serverConfig.properties");
	    add("serverconfig.properties");
	    add("serverConfiguration.properties");
	    add("serverconfiguration.properties");
	    add("server-config.properties");
	    add("configs.properties");
	    add("configuration.properties");
	    add("configurations.properties");
	    add("parameters.properties");
	    add("app.properties");
	    add("application.properties");
	    add("appconfig.properties");
	    add("applicationConfig.properties");
	    add("applicationConfiguration.properties");
	    add("appconfiguration.properties");
	}
    };

    /**
     * List of common paths to look for the files when needed. 
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     */
    public static Set<String> commonPaths = new HashSet<>() {
	{
	    add("");
	    add("" + File.separatorChar);
	    add(".." + File.separatorChar);
	    add(File.separatorChar + ".." + File.separatorChar);
	    add("src" + File.separatorChar + "main" + File.separatorChar + "resources");
	    add(File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources");
	    add("main" + File.separatorChar + "resources" + File.separatorChar + "");
	    add(File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "");
	    add("resources" + File.separatorChar + "");
	    add(File.separatorChar + "resources" + File.separatorChar + "");
	}
    };

    /**
     * Set of mapped properties that stores pairs of property:value.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     */
    public static Set<HashMap<String, Object>> propertiesSet = new LinkedHashSet<>();
    
    /**
     * Set of strategies that look into the application for the properties files.  
     * Annotations should do this trick.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-10-03
     */
    public static HashSet<PropertiesFileRetrievingStrategy> strategies=new HashSet<>(){{
	add(new ClassLoaderStrategy());
	add(new FileInputStreamStrategy());
	add(new ClazzStreamStrategy());
	add(new CustomLoaderStrategy());
	add(new OutOfBuildStrategy());
    }};

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Methods">

    //<editor-fold defaultstate="collapsed" desc="public methods">
    
    /**
     * It ensures that the file is loaded in the {@link #propertiesSet} and then looks for the value in it.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param propertyName	    Name of the property to look for.
     * @param propertiesFileName    Name of the properties file to look for the property into.
     * @return value of the property with the given name.
     */
    public static Object getProperty(String propertyName, String propertiesFileName) {
	loadPropertiesFile(propertiesFileName);
	Object property = getProperty(propertyName);
	return property;
    }

    /**
     * Core method, it returns the value of property loaded on the {@link #propertiesSet} of the application. Returns null if not found, if the {@link #propertiesSet} hasn't been 
     * loaded it launches the process that does it.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param propertyName Name of the property to look for.
     * @return value of the property with the given name.
     */
    public static Object getProperty(String propertyName) {
	logger.trace("::getProperty(propertyName) - Start: Looking for property");
	try {
	    Object propertyValue = null;
	    for (HashMap<String, Object> propertiesElement : getLoadedProperties()) {
		propertyValue = propertiesElement.get(propertyName);
		if (propertyValue != null) {
		    break;
		}
	    }
	    if (propertyValue == null) {
		logger.warn("::getProperty(propertyName) - Unable to find the requested property:[{}] in any of the Property files loaded",propertyName);
	    }

	    logger.trace("::getProperty(propertyName) - Finish: Returning the property value");
	    return propertyValue;
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Unknown error while looking for the property", ex);
	}
    }

    //</editor-fold>
    
    /**
     * Will add the provided custom property with the rest of the properties loaded from files.	
     * 
     * @author <a href='mailto:obed.vazquez@gmail.com'>Obed Vazquez</a>
     * @since 2021-02-01
     * @param propertyKey   Property name.
     * @param propertyValue {@link Object} to add as a value of the property.
     * @throws IllegalArgumentException - if the provided parameter is null.
     */
    public static void loadCustomProperty(String propertyKey, Object propertyValue) {
	log.trace("::loadManualProperty(propertyKey,propertyValue) - Start: ");
	notNullValidation(propertyKey,propertyValue);
	try{
	    
	    propertiesSet.add(new HashMap<String,Object>(){{put(propertyKey,propertyValue);}});

	    log.trace("::loadManualProperty(propertyKey,propertyValue) - Finish: ");
	} catch (Exception e) {
            throw new RuntimeException("Impossible to complete the operation due to an unknown internal error.", e);
        }
    }
    /**
     * Bridge Method to {@link #loadPropertiesFile(java.lang.String, java.lang.Class)}. Just set up  default class.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param propertiesFileName Name of the file of the properties to load.
     */
    public static void loadPropertiesFile(String propertiesFileName) {
	loadPropertiesFile(propertiesFileName,PropertiesManager.class);
    }

    /**
     * Loads an extra {@link HashMap} of properties in the {@link #propertiesSet} with the properties of the app.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param propertiesFileName Name of the file of the properties to load.
     * @param referenceClass Reference Class to use as a reference to get the files.
     */
    public static void loadPropertiesFile(String propertiesFileName,Class referenceClass){
	logger.trace("::loadPropertiesFile(propertiesFileName,referenceClass) - Start: adding the file properties to the application propertiesSet");
	try {
	    Set<HashMap<String, Object>> newProperties = getProperties(propertiesFileName,referenceClass==null?PropertiesManager.class:referenceClass);
	    if(propertiesSet==null || propertiesSet.isEmpty()) propertiesSet = getDefaultProperties();
	    propertiesSet.addAll(newProperties);
	    logger.trace("::loadPropertiesFile(propertiesFileName,referenceClass) - Finish: properties added");
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Impossible to add the new properties file properties to the application propertiesSet.", ex);
	}
    }
    
    /**
     * Obtains the already loaded properties of the application or if they have not been loaded it loads it with {@link #getDefaultProperties()} method help before returning them.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @return The {@link Set} with the app loaded properties.
     */
    public static Set<HashMap<String, Object>> getLoadedProperties() {
	logger.trace("::getLoadedProperties() - Start: obtaining application properties");
	try {
	    if (propertiesSet == null || propertiesSet.isEmpty()) {
		logger.debug("::getLoadedProperties() - properties are not yet loaded, loading from default...");
		propertiesSet = getDefaultProperties();
	    }
	    logger.trace("::getLoadedProperties() - Finish: returning properties loaded");
	    return propertiesSet;
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Unable to obtain the loaded properties", ex);
	}
    }
    
    /**
     * Loads all default properties calling method {@link PropertiesManager#getDefaultProperties()} and adding them to the {@link PropertiesManager#propertiesSet} already loaded.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     */
    public static void loadDefaultProperties(){
	logger.trace("::loadDefaultProperties() - Start: loading default properties");
	try{
	    Set<HashMap<String,Object>> defaultProperties=getDefaultProperties();
	   propertiesSet.addAll(defaultProperties);
	logger.trace("::loadDefaultProperties() - Finish: default properties loaded");
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Unable to load default properties", ex);
	}
    }

    /**
     * Just obtains all the default properties or empty if a problem occurs.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @return		{@link Set} with the loaded properties.
     */
    public static Set<HashMap<String, Object>> getDefaultProperties() {
	logger.trace("::getDefaultProperties() - Start: Looking for default Properties");
	try{
	    Set<HashMap<String, Object>> defaultProperties;
	    try {
		defaultProperties = getAllProperties();//same as calling: getProperties(null, null)
	    } catch (Exception ex) {
		logger.warn("::getDefaultProperties() - Impossible to find any common properties file on the project, loading empty properties");
		defaultProperties = new HashSet<>();
	    }
	    logger.trace("::getDefaultProperties() - Finish: Returning default Properties");
	    return defaultProperties;
	}catch (Exception ex) {
	    throw new PropertiesManagerException("Unable to obtain the default properties", ex);
	}
    }
    
    
    /**
     * Bridge Method to {@link #getProperties(java.lang.String, java.lang.String)}. Just set up <code>path=null</code>
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param fileName	name of the properties file to look for.
     * @return		{@link HashMap} with the loaded properties.
     */
    public static Set<HashMap<String, Object>> getProperties(String fileName) {
	Set<HashMap<String, Object>> properties = getProperties(null, fileName);
	return properties;
    }
    
    /**
     * Bridge Method to {@link #getProperties(java.lang.String, java.lang.String)}. Just set up <code>path=null</code> and default class.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param fileName	name of the properties file to look for.
     * @param referenceClass Reference Class to use as a reference to get the files.
     * @return		{@link HashMap} with the loaded properties.
     */
    public static Set<HashMap<String, Object>> getProperties(String fileName,Class referenceClass) {
	Set<HashMap<String, Object>> properties = getProperties(null, fileName,referenceClass==null?PropertiesManager.class:referenceClass);
	return properties;
    }

    /**
     * Bridge method to default the class instance.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param path	Path to look for the file.
     * @param fileName	name of the properties file to look for.
     * @return a {@link HashMap} with the properties represented on it.
     */
    public static Set<HashMap<String, Object>> getProperties(String path, String fileName) {
	Set<HashMap<String, Object>> propertiesSet=getProperties(path, fileName,PropertiesManager.class);
	return propertiesSet;
    }
    
    /**
     * Obtains a {@link HashMap} with the mapped specified file name properties loaded into it. If no filename is provided combines methods {@link #getAllPropertiesISOnProject()} 
     * and {@link #getAllProperties()} to assure the obtainment of the files.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param path	Path to look for the file.
     * @param fileName	name of the properties file to look for.
     * @param referenceClass Reference Class to use as a reference to get the files.
     * @return		{@link HashMap} with the loaded properties.
     */
    public static Set<HashMap<String, Object>> getProperties(String path, String fileName, Class referenceClass) {
	logger.trace("::getProperties(path,fileName,referenceClass) - Start: Obtaining the Properties");
	try {
		    
	    if(fileName == null)
		return getAllProperties(referenceClass==null?PropertiesManager.class:referenceClass);
	    
	    Set<HashMap<String, Object>> propertiesSet=new HashSet<>();
	    
	    InputStream inputStream= path == null ? getPropertiesIS(fileName,referenceClass==null?PropertiesManager.class:referenceClass) : 
							getPropertiesIS(path, fileName,referenceClass==null?PropertiesManager.class:referenceClass);
	    if (inputStream != null) {
		HashMap<String, Object> prop=mapProperties(inputStream);
		if(prop!=null && prop.size()>0) propertiesSet.add(prop);
	    } else {
		logger.info("::getProperties(path,fileName,referenceClass) - Couln't find the specified properties file by any method.");//Returns empty properties
	    }

	    logger.trace("::getProperties(path,fileName,referenceClass) - Finish: Returning all properties");
	    return propertiesSet;

	} catch (Exception ex) {
	    throw new PropertiesManagerException("Impossible to map the properties file to in-memory Properties object", ex);
	}
    }
    
    /**
     * Bridge method to default the class instance.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @return a {@link HashMap} with the properties represented on it.
     */
    public static Set<HashMap<String, Object>> getAllProperties(){
	Set<HashMap<String, Object>> propertiesSet=getAllProperties(PropertiesManager.class);
	return propertiesSet;
    }
    
    /**
     * This method combines all the common name files and a method to search for all properties files in the project to get all possible options to obtain properties files 
     * from the calling project. If it gets a duplicated property on the same or different files will just run with it.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param referenceClass Reference Class to use as a reference to get the files.
     * @return a {@link HashMap} with the properties represented on it.
     */
    public static Set<HashMap<String, Object>> getAllProperties(Class referenceClass){
	logger.trace("::getAllProperties(referenceClass) - Start: Looking for all properties files in the project");
	try{
	    Set<HashMap<String, Object>> propertiesSet=new HashSet<>();
	    HashSet<InputStream> inputStreams;
	    try{
		inputStreams = getInputStreamFromCommonFileNames(referenceClass==null?PropertiesManager.class:referenceClass); //not sure if looking for all works, so this is looking for the common files also.
	    }catch(Exception ex){
		inputStreams=null;
	    }
	    if(inputStreams==null || inputStreams.size()<1){//looking for all
		inputStreams=getAllPropertiesISOnProject();
	    }else{
		for(InputStream inputStream:inputStreams){
		    HashMap<String, Object> prop=mapProperties(inputStream);
		    if(prop!=null && prop.size()>0) propertiesSet.add(prop);
		}
		inputStreams=getAllPropertiesISOnProject();
	    }
	    if(inputStreams!=null){
		/*
		for(InputStream inputStream:getAllPropertiesISOnProject()){
		    HashMap<String, Object> prop=mapProperties(inputStream);
		    if(prop!=null && prop.size()>0) propertiesSet.add(prop);
		} 
		*/
		//Functional operation has better performance but it is harder to read
		getAllPropertiesISOnProject().stream().map((inputStream) -> mapProperties(inputStream)).filter((prop) -> (prop!=null && prop.size()>0)).forEachOrdered((prop) -> {
		    propertiesSet.add(prop);
		});
	    }
	    logger.trace("::getAllProperties(referenceClass) - Finish: Returning all properties");
	    return propertiesSet;
	}catch(Exception ex){
	    throw new PropertiesManagerException("Impossible to map All the properties files in project to an in-memory Properties object", ex);
	}
    }
    
    /**
     * It scans the project looking for any .properties file and returns its {@link InputStream}.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @return {@link HashSet} loaded with the .properties files {@link InputStream}s, or empty if nothing is found.
     */
    public static HashSet<InputStream> getAllPropertiesISOnProject(){
	    logger.trace("::getAllPropertiesISOnProject() - Start: Getting all the .properties files in the project");
	
	String projectPath=System.getProperty("user.dir");
	try (Stream<Path> paths = Files.walk(Paths.get(projectPath))) {
	    
	    HashSet<InputStream> inputStreams=new HashSet<>();
	    
	    paths.filter(Files::isRegularFile)
		    .forEach((Path path)->{
			try{
			    if(path.endsWith(".properties"))
				inputStreams.add(Files.newInputStream(path));
			}catch(Exception ex){
			    logger.warn("::getAllPropertiesISOnProject() - Error while trying to obtain a file, the file will be ignored but please report this exception.");
			}
		    });
	    
	    logger.trace("::getAllPropertiesISOnProject() - Finish: Returning all properties files in the project");
	    return inputStreams;
	    
	} catch(Exception ex){
	    throw new PropertiesManagerException("Imposible to get all files in the project", ex);
	}
    }
    
    /**
     * Bridge method to default the class instance.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @return {@link InputStream} for each of the common properties files found.
     */
    public static HashSet<InputStream> getInputStreamFromCommonFileNames(){
	HashSet<InputStream> is=getInputStreamFromCommonFileNames(PropertiesManager.class);
	return is;
    }

    /**
     * It obtains the {@link InputStream} instances of the known common names (stored at {@link #commonPropertiesFileNames}) for the proper
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param referenceClass Reference Class to use as a reference to get the files.
     * @return {@link InputStream} for each of the common properties files found.
     */
    public static HashSet<InputStream> getInputStreamFromCommonFileNames(Class referenceClass){
	logger.trace("::getInputStreamFromCommonFileNames(referenceClass) - Start");
	try{
	    HashSet<InputStream> inputStreams= new HashSet<>();
	    commonPropertiesFileNames.forEach((commonPropertiesFileName) -> {
		InputStream inputStream;
		try {
		    logger.debug("::getInputStreamFromCommonFileNames(referenceClass) - Looking for file: " + commonPropertiesFileName);
		    inputStream = referenceClass==null? getPropertiesIS(commonPropertiesFileName,PropertiesManager.class):getPropertiesIS(commonPropertiesFileName,referenceClass);
		    if (inputStream != null) {
			inputStreams.add(inputStream);
		    }
		} catch (RuntimeException ex) {
		    logger.debug("::getInputStreamFromCommonFileNames(referenceClass) - File not found. Couldn't obtain file: " + commonPropertiesFileName);
		}
	    });
	    if (inputStreams.size()<1)
		logger.info("::getInputStreamFromCommonFileNames(referenceClass) - Unable to find any file with any of the commonFileNames");
		
	    logger.trace("::getInputStreamFromCommonFileNames(referenceClass) - Finish: returning the file");
	    return inputStreams;
	}catch(Exception ex){
	    throw new PropertiesManagerException("Imposible to get InputStream objects from common name files due to an error.", ex);
	}
    }
    
    /**
     * Maps data from a properties {@link InputStream} into a {@link HashMap}.
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-13
     * @param inputStream   {@link InputStream} to map.
     * @return		    {@link HashMap} with the data loaded.
     */
    public static HashMap<String, Object> mapProperties(InputStream inputStream){
	logger.trace("::mapProperties(inputStream) - Start: Mapping properties");
	try{
	    HashMap<String, Object> propertiesMap = new HashMap<>();
	    if (inputStream != null) {
		Properties properties = new Properties();
		properties.load(inputStream);

		properties.forEach((key, value) -> {
		    propertiesMap.put(key.toString(), value.toString());
		});
	    }
	    logger.trace("::mapProperties(inputStream) - Finish: Returning properties");
	    return propertiesMap;
	}catch(Exception ex){
	    throw new PropertiesManagerException("Impossible to map the properties IS to an in-memory Properties instance", ex);
	}
    }
    
    //<editor-fold defaultstate="collapsed" desc="InputStream Getters">


    /**
     * Bridge method to the one that has the collection of all methods known to obtain a {@link File} or ultimately {@link InputStream}.
     *
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @param fileName	The name of the file to obtain as an {@link InputStream}
     * @return	The file for the provided file name as an {@link InputStream}
     */
    public static InputStream getPropertiesIS(String fileName) {
	logger.trace("::getInputStreamFrom(fileName) - Start: Obtaining the file");
	try {
	    InputStream inputStream;
	    inputStream = getPropertiesIS(null, fileName,PropertiesManager.class);
	    logger.trace("::getInputStreamFrom(fileName) - Finish: Returning File");
	    return inputStream;
	} catch (Exception ex) {
	    logger.error("::getInputStreamFrom(fileName) - Error while trying to get InputStream from fileName");
	    throw new PropertiesManagerException("Error while trying to get InputStream from fileName", ex);
	}
    }
    
    /**
     * This has the collection of all methods known to obtain a {@link File} or ultimately {@link InputStream}.
     *
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-12
     * @param fileName	The name of the file to obtain as an {@link InputStream}
     * @param clazz	{@link Class} to use as a reference, if null will use local class instead.
     * @return	The file for the provided file name as an {@link InputStream}
     */
    public static InputStream getPropertiesIS(String fileName, Class clazz) {
	logger.trace("::getPropertiesIS(fileName,clazz) - Start: Obtaining the file");
	try {
	    InputStream inputStream;
	    inputStream = getPropertiesIS(null, fileName,(clazz==null?PropertiesManager.class:clazz));
	    logger.trace("::getPropertiesIS(fileName,clazz) - Finish: Returning File");
	    return inputStream;
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Error while trying to get InputStream from fileName", ex);
	}
    }
    
    /**
     * Bridge method to the one that has the collection of all methods known to obtain a {@link File} or ultimately {@link InputStream}.
     *
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-12
     * @param path	path where to find the file to search for.
     * @param fileName	The name of the file to obtain as an {@link InputStream}
     * @return	The file for the provided file name as an {@link InputStream}
     */
    public static InputStream getPropertiesIS(String path, String fileName) {
	logger.trace("::getInputStreamFrom(fileName) - Start: Obtaining the file");
	try {
	    InputStream inputStream;
	    inputStream = getPropertiesIS(path, fileName,PropertiesManager.class);
	    logger.trace("::getInputStreamFrom(fileName) - Finish: Returning File");
	    return inputStream;
	} catch (Exception ex) {
	    logger.error("::getInputStreamFrom(fileName) - Error while trying to get InputStream from fileName");
	    throw new PropertiesManagerException("Error while trying to get InputStream from fileName", ex);
	}
    }

    /**
     * This has the collection of all methods known to obtain a {@link File} or ultimately {@link InputStream}.
     *
     * @author <a href="mailto:obed.vazquez@gmail.com">Obed Vazquez</a>
     * @since 2019-02-12
     * @param path	path where to find the file to search for.
     * @param fileName	The name of the file to obtain as an {@link InputStream}
     * @param clazz	{@link Class} to use as a reference, if null will use local class instead.
     * @return	The file for the provided file name as an {@link InputStream}
     */
    public static InputStream getPropertiesIS(String path, String fileName, Class clazz) {
	logger.trace("::getPropertiesIS(path,fileName,clazz) - Start: Obtaining the file");
	try {
	    
	    InputStream inputStream=null;
	    
	    for(PropertiesFileRetrievingStrategy strategy:strategies){ //goes through all strategies
		try{
		    logger.debug("::getPropertiesIS(path,fileName,clazz) - Trying to obtain file:{} at:{} with class:{}", fileName, path,clazz);
		    if(strategy instanceof PropertiesFileRetrievingThroughClassStrategy){
			inputStream=((PropertiesFileRetrievingThroughClassStrategy)strategy).
				getPropertiesIS(path, fileName,(clazz==null?PropertiesManager.class:clazz));
		    }else if(strategy!=null){
			inputStream=strategy.getPropertiesIS(path, fileName);
		    }
		
		}catch(PropertiesManagerException ex){
		    inputStream=null;
		}
		if(inputStream!=null) break; //strategy worked
	    }
	    

	    logger.trace("::getPropertiesIS(path,fileName,clazz) - Finish: Returning File");
	    return inputStream;
	} catch (Exception ex) {
	    throw new PropertiesManagerException("Error while trying to get InputStream from fileName", ex);
	}
    }

    //</editor-fold>
    
    //</editor-fold>
    
}
