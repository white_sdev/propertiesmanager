/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.white_sdev.propertiesmanager;

import java.util.HashSet;
import org.junit.jupiter.api.Test;
import org.white_sdev.propertiesmanager.model.service.PropertiesManager;

/**
 *
 * @author WhitePC
 */
public class PropertiesManagerTest {

    public PropertiesManagerTest() {
    }

    private void init() {
	setLogLevel("trace");
	PropertiesManager.propertiesSet=new HashSet<>();
    }


    @Test
    public void testUsualRetrieve() {
	init();
	
	Object property=PropertiesManager.getProperty("common");
	assert(property!=null);
    }
    
    @Test
    public void testCusomLoadAndretrieveProperty() {
	init();
	
	assert(PropertiesManager.getProperty("custom","as.properties")!=null);
    }
    
    @Test
    public void testCustomLoad(){
	init();
	
	PropertiesManager.loadPropertiesFile("test3.properties");
	assert(PropertiesManager.getProperty("test3property")!=null);
    }
    
    @Test
    public void testIntegration(){
	init();
	
	
	PropertiesManager.loadPropertiesFile("test3.properties");
	Object property=PropertiesManager.getProperty("test3property");
	assert(property!=null);
	
	PropertiesManager.loadDefaultProperties();
	property=PropertiesManager.getProperty("common");
	assert(property!=null);
	
	
	property=PropertiesManager.getProperty("test3property");
	assert(property!=null);
    }
    
    
    private void setLogLevel(String level){
	System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, level);
    }

}
